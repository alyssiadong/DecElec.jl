using DecElec
using Test

using OSQP

@testset "Parse csv files" begin
    testcasefile = "data/n110_p30_c80_0x000f.csv"
    parsed_file = DecElec.parse_csv(testcasefile)

    @test true
end

@testset "instantiate models" begin
    testcasefile = "data/n110_p30_c80_0x000f.csv"
    data = DecElec.parse_csv(testcasefile)

    # instantiate a group of P2P agents
    group = DecElec.instantiate_model(data, 
                    DecElec.P2PProblem, 
                    DecElec.GroupOfAgents(), 
                    DecElec.build_group, DecElec.ref_add_global_core!)

    @test true
end

@testset "p2p problem" begin
    testcasefile = "data/n110_p30_c80_0x000f.csv"
    demo, final_sol = DecElec.run_p2p(testcasefile; iter_max = 1000)
    optimal_res = DecElec.optimal_result(demo)

    @test true
end