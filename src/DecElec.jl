module DecElec

# import PowerModels
# import PowerModels: _MOI
# import PowerModels: AbstractPowerModel, AbstractACRModel, AbstractACPModel
# import PowerModels: ACRPowerModel, ACPPowerModel
# import PowerModels: constraint_model_voltage
# import PowerModels: optimize_model!, instantiate_model, comp_start_value, ref_calc_branch_flow_bounds
# import PowerModels: ids, ref, var, con, sol, nw_ids, nws
# import PowerModels: pm_it_name, pm_it_sym

# import InfrastructureModels
# import InfrastructureModels: @im_fields, nw_id_default
# const _IM = InfrastructureModels

import CSV
import DataFrames
import DataFrames: DataFrame

import JuMP
import Ipopt
import OSQP

import AxisArrays
import LinearAlgebra
import LinearAlgebra: norm, Diagonal
import SparseArrays: I, sparse
import LightGraphs: SimpleGraph, add_edge!, neighbors, nv, ne

import PlotlyJS
import PlotlyJS: plot, scatter, GenericTrace, Layout

import Memento

# Create our module level logger (this will get precompiled)
const _LOGGER = Memento.getlogger(@__MODULE__)

# Register the module level logger at runtime so that folks can access the logger via `getlogger(PowerModels)`
# NOTE: If this line is not included then the precompiled `PowerModels._LOGGER` won't be registered at runtime.
__init__() = Memento.register(_LOGGER)

"Suppresses information and warning messages output by PowerModels, for fine grained control use the Memento package"
function silence()
    Memento.info(_LOGGER, "Suppressing information and warning messages for the rest of this session.  Use the Memento package for more fine-grained control of logging.")
    # Memento.setlevel!(Memento.getlogger(InfrastructureModels), "error")
    # Memento.setlevel!(Memento.getlogger(PowerModels), "error")
    Memento.setlevel!(Memento.getlogger(DecElec), "error")
end

"alows the user to set the logging level without the need to add Memento"
function logger_config!(level)
    Memento.config!(Memento.getlogger("DecElec"), level)
end

include("core/base.jl")
include("core/type.jl")
include("core/mon.jl")
include("core/ref.jl")
include("core/agent.jl")
include("core/group.jl")
include("core/data.jl")
include("core/vis.jl")

include("io/csv.jl")

include("prob/general.jl")
include("prob/p2p.jl")

end
