function run_problem(demo::AbstractDecElecProblem)
	first_iter(demo)
	while !convergence_reached(demo)
		iter(demo)
	end
	return finalize(demo)
end

function iter(demo::AbstractDecElecProblem)
	iter_computation(demo)
	iter_communication(demo)
	update_problem(demo)
	update_monitor(demo)
end

function finalize(demo::AbstractDecElecProblem)
	# callback function for final solution and monitoring
	truncate_monitor(demo)
	return build_sol_dic(demo)
end