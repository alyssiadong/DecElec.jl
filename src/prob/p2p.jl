function run_p2p(file; kwargs...)
	data = parse_csv(file)
	demo = instantiate_model(data, P2PProblem, GroupOfAgents(), 
		build_group, ref_add_global_core!; kwargs)

	final_sol = run_problem(demo)
	demo, final_sol
end

"""
Iteration functions
"""
first_iter(demo::P2PProblem) = iter(demo)

function iter_computation(demo::P2PProblem)
	for (n, agent) in demo.content.agents
		iter_computation(agent)
	end
end
function iter_computation(agent::P2PAgent)
	m = agent.content.model

	results = OSQP.solve!(m)            # Solve !
	!(results.info.status == :Solved) && println(results.info.status)
	T_sol = results.x                   # x = Arg min(problem)

	i = ref(agent, :prosumer, "id_agent")
	for (k,idx) in enumerate(agent.var[:trade])
		agent.sol["trade_prev"][idx] = sol(agent, "trade", idx)
		agent.sol["trade"][idx] = T_sol[k]
	end
	agent.sol["power"][i] = sum(T_sol)
end


function iter_communication(demo::P2PProblem)
	for (n, agent) in demo.content.agents
		for (n_arc, i, j) in agent.var[:trade]
			# @assert n == i "n=$n et i=$i"
			tij = sol(agent, "trade", (n_arc, i, j))
			agent_j = demo.content.agents[j]
			agent_j.sol["trade"][(n_arc, i, j)] = tij
		end
	end
end


function update_problem(demo::P2PProblem)
	for (n, agent) in demo.content.agents
		update_problem(agent)
	end
	# computes global residuals
	demo.sol["primal_res"][0] 	= sum( sum(sol(agent, "trade_primal_res",idx) for idx in var(agent, :trade_primal_res)) 	for (n, agent) in demo.content.agents)
	demo.sol["dual_res"][0] 	= sum( sum(sol(agent, "trade_dual_res",idx)   for idx in var(agent, :trade_dual_res))		for (n, agent) in demo.content.agents)
end
function update_problem(agent::P2PAgent)
	arcs = ref(agent, :commercial_arcs)
	ρ = ref(agent, :prosumer, "rho")
	gencost_a, gencost_b, gencost_c = ref(agent, :prosumer, "costcoef")
	
	# updates dual values
	for idx in var(agent, :trade_dual)
		n_arc, i, j = idx
		agent.sol["trade_dual"][idx] = sol(agent,"trade_dual",idx) - ρ/2*( sol(agent, "trade", idx) + sol(agent, "trade", (n_arc, j, i)) )
	end

	# updates OSQP model with new values
	q = [gencost_b - ρ*(sol(agent, "trade", (n_arc, i, j)) - sol(agent, "trade", (n_arc, j, i)))/2 - sol(agent, "trade_dual", (n_arc, i, j)) for (n_arc, i, j)∈arcs]
	m = agent.content.model
	OSQP.update_q!(m, q)

	# computes local residuals
	for idx in var(agent, :trade_primal_res)
		n_arc, i, j = idx
		agent.sol["trade_primal_res"][idx] 	= (sol(agent, "trade", idx) + sol(agent, "trade", (n_arc, j, i)) )^2
		agent.sol["trade_dual_res"][idx]	= (sol(agent, "trade", idx) - sol(agent, "trade_prev", idx) )^2
	end
end




"""
Final solution dictionary
"""
function build_sol_dic(demo::P2PProblem)
	sol = Dict{String,Any}()
	sol["primal_res"] = demo.sol["primal_res"][0]
	sol["dual_res"] = demo.sol["dual_res"][0]
	sol["power"] = Dict()
	sol["trade"] = Dict()
	for (n, agent) in demo.content.agents
		sol["power"][n] = agent.sol["power"][n]
		sol["trade"] 	= merge(sol["trade"], Dict((i, j) => value for ((n_arc,i,j),value) in agent.sol["trade"] if i==n))
	end

	demo.ext[:final_sol] = sol
	return sol
end

"""
Checking for convergence or max iteration reached
"""
function convergence_reached(demo::P2PProblem)
	# primal condition only
	ε_prim = ref(demo, :ε_prim)
	convergence = (sol(demo, "primal_res",0) < ε_prim)
	max_iter = (demo.mon["current_iter"] > 1000)
	return convergence | max_iter
end


"""
Optimal centralized result for comparison
"""
function optimal_result(demo::P2PProblem)
    ### Solves the problem in a centralized, synchronous way,
    ### using OSQP solver.
    ### OSQP solves quadratic problem that has the form :
    ###         min x'Px + q'x
    ###         s.t. l ≤ Ax ≤ u
    ### with P and A sparse matrix

    N_Ω = length(demo.data["prosumer"])
    an = 	[ demo.data["prosumer"][n]["costcoef"][1] 	for n in 1:N_Ω]
    bn = 	[ demo.data["prosumer"][n]["costcoef"][2] 	for n in 1:N_Ω]
    pmin = 	[ demo.data["prosumer"][n]["pmin"] 			for n in 1:N_Ω]
    pmax =  [ demo.data["prosumer"][n]["pmax"] 			for n in 1:N_Ω]

    P = sparse(4 .*Diagonal(an))
    q = bn

    A = sparse(vcat(ones(N_Ω)',Matrix(1.0I,N_Ω,N_Ω)))
    l = vcat(0.0,pmin)
    u = vcat(0.0,pmax)

    m = OSQP.Model()
    OSQP.setup!(m, P=P, q=q,
        A=A, l=l, u=u, verbose=false)   # Model setup with the right arrays
    results = OSQP.solve!(m)            # Solve !
    T_sol = results.x                   # x = Arg min(problem)

    demo.ext[:optimal_sol] = T_sol
    return T_sol
end