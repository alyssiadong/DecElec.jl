"CSV files are only used for P2P problem data. Otherwise, we use matpower files."

"Parsing raw data from csv file, out: dataframe"
function raw_parse_csv(filepath::String)
	@assert isfile(filepath) "Filepath is not correct."
	return CSV.read(filepath, DataFrame)
end

"Parsing P2P problem data from csv file, out: dictionary"
function parse_csv(filepath::String)
	raw_data = raw_parse_csv(filepath)

	# in P2P problem, the only available informations are prosumer ones
	out_dic = Dict{String,Any}("prosumer" => Dict{Int,Any}())
	for dfrow in eachrow(raw_data)
		out_dic["prosumer"][dfrow.id_agent] = prosumer_dic = Dict{String,Any}()
		prosumer_dic["id_agent"] 	= dfrow.id_agent
		prosumer_dic["type"] 		= dfrow.type
		prosumer_dic["pmin"] 		= dfrow.Pmin
		prosumer_dic["pmax"] 		= dfrow.Pmax
		prosumer_dic["loc"]  		= [ dfrow.loc_x, dfrow.loc_y ]
		prosumer_dic["costcoef"]  	= [ dfrow.gencost_a, dfrow.gencost_b, 0.0 ]
		prosumer_dic["rho"] 		= dfrow.rho
		prosumer_dic["gamma"] 		= hasproperty(dfrow, :gamma) ? dfrow.gamma : 0.0
	end

	out_dic["filepath"] = filepath
	return out_dic
end