"Single agents specific functions"

"extend_model: Does nothing if the model is already a singular agent (OR instantiate each agent of the group of agents)"
extend_model(demo::AbstractDecElecAgent) = nothing

"build_agent for P2PAgent: variable initialization and OSQP problem setup"
function build_agent(demo::P2PAgent, iter_max::Int)

	### Variables setup: tij for each commercial arc and pi: local power
	i = ref(demo, :prosumer, "id_agent")
	arcs = ref(demo, :commercial_arcs)
	N_ωi = length(arcs)

	demo.var[:trade]  = arcs
	demo.var[:trade_dual]  = arcs
	demo.var[:trade_primal_res] = arcs
	demo.var[:trade_dual_res] = arcs

	demo.var[:power]  		= [i]
	demo.var[:primal_res] 	= [i]
	demo.var[:dual_res] 	= [i]

	temp1 = Dict(arc => 0.0 for arc in arcs)
	temp2 = Dict((n_arc, j, i) => 0.0 for (n_arc, i, j) in arcs)
	demo.sol["trade"] = merge(temp1, temp2)
	demo.sol["trade_prev"] = merge(temp1, temp2)

	demo.sol["trade_dual"] = Dict(arc => 0.0 for arc in arcs)
	demo.sol["power"] = Dict( i  => 0.0)

	demo.sol["trade_primal_res"] 	= Dict(arc => Inf for arc in arcs)
	demo.sol["trade_dual_res"] 		= Dict(arc => Inf for arc in arcs)


	### Using OSQP to solve the local problem.
	### OSQP solves quadratic problem that has the form :
	###         min x'Px + q'x
	###         s.t. l ≤ Ax ≤ u
	### with P and A sparse matrix
	ρ = ref(demo, :prosumer, "rho")
	γ = ref(demo, :prosumer, "gamma")
	type = ref(demo, :prosumer, "type")
	gencost_a, gencost_b, gencost_c = ref(demo, :prosumer, "costcoef")
	pmin = ref(demo, :prosumer, "pmin")
	pmax = ref(demo, :prosumer, "pmax")

	OSQPeps = 1e-5

	# Setting OSQP matrix
	P1 = ones(N_ωi, N_ωi).*4*gencost_a + ρ*I
	P2 = Matrix(2*γ*I, N_ωi, N_ωi)
	P = sparse(P1+P2)

	q = [gencost_b -
			ρ*(sol(demo, "trade", (n_arc, i, j)) - sol(demo, "trade", (n_arc, j, i)))/2 -
			sol(demo, "trade_dual", (n_arc, i, j)) for (n_arc, i, j)∈arcs]

	A1 = ones(1, N_ωi)
	A2 = Matrix(1.0I, N_ωi, N_ωi)
	A = sparse(vcat(A1,A2))

	# Power and trade limitations
	if type == "cons"
	    l = Float64.(vcat(pmin, [ pmin for idx∈arcs]))
	    u = Float64.(vcat(pmax, [ 0.0  for idx∈arcs]))
	elseif type == "prod"
	    l = Float64.(vcat(pmin, [ 0.0  for idx∈arcs]))
	    u = Float64.(vcat(pmax, [ pmax for idx∈arcs]))
	end

	# OSQP model setup
	m = demo.content.model
	OSQP.setup!(m, P=P, q=q,
	    A=A, l=l, u=u, verbose=false,
	    eps_rel = OSQPeps, eps_abs = OSQPeps)

	# Monitor setup
	mon_initialize(demo, iter_max)
end