"""
Builds an initial "mon" dictionary converting strings to symbols and component
keys to integers. 
"""
function mon_initialize(demo::AbstractDecElecContainer, nmax)
    var = demo.var
    for (key, items) in var
        demo.mon[String(key)] = Dict( item => zeros(nmax) for item in items )
    end
    demo.mon["current_iter"] = 1

    return true
end





"""
Updates "mon" dictionary with new data
"""
function update_monitor(demo::AbstractDecElecProblem)
    # increase global monitor iteration
    demo.mon["current_iter"] += 1

    # copy values in mon
    _update_monitor!(demo.mon, demo.sol, demo.mon["current_iter"])

    # update each individual monitor
    for (n, agent) in demo.content.agents
        update_monitor(agent)
    end
end
function truncate_monitor(demo::AbstractDecElecProblem)
    # global monitor
    _truncate_monitor(demo)

    # local monitors
    for (n, agent) in demo.content.agents
        _truncate_monitor(agent)
    end
end

function update_monitor(agent::AbstractDecElecAgent)
    # increase agent monitor iteration
    agent.mon["current_iter"] += 1

    # copy values in mon
    _update_monitor!(agent.mon, agent.sol, agent.mon["current_iter"])
end

"recursive call of _update_data"
function _update_monitor!(monitor::Dict, new_data::Dict, current_iter::Int)
    for (key, v) in monitor
        if haskey(new_data, key)
            new_v = new_data[key]
            if isa(v, Dict) && isa(new_v, Dict)
                _update_monitor!(v, new_v, current_iter)
            else
                monitor[key][current_iter] = new_v
            end
        else
            # Memento.warn(_LOGGER, "Warning, $key not found in new_data dict.")
        end
    end
end

"""
Truncate "mon" arrays to fit saved data
"""
function _truncate_monitor(demo::AbstractDecElecContainer)
    current_iter = demo.mon["current_iter"]
    for (k,dic) in demo.mon
        if k == "current_iter"
            nothing
        else
            for (idx, value) in dic
                dic[idx] = value[1:current_iter]
            end
        end
    end
end