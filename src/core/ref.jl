"Add global data to ref "
function ref_add_global_core!(ref::Dict{Symbol,<:Any})
	if haskey(ref, :prosumer) 		# data from csv
		ref[:prosumer_prod] 	= [k for (k,v) in ref[:prosumer] if v["type"] == "prod"]
		ref[:prosumer_cons] 	= [k for (k,v) in ref[:prosumer] if v["type"] == "cons"]

	# copied from PowerModels
	elseif haskey(ref, :bus)
		Memento.warn(_LOGGER, "Vérifier cette partie de ref_add_global_core!")
	    
	    if !haskey(ref, :conductor_ids)
	        if !haskey(ref, :conductors)
	            ref[:conductor_ids] = 1:1
	        else
	            ref[:conductor_ids] = 1:ref[:conductors]
	        end
	    end

	    ### filter out inactive components ###
	    ref[:bus] = Dict(x for x in ref[:bus] if (x.second["bus_type"] != pm_component_status_inactive["bus"]))
	    ref[:load] = Dict(x for x in ref[:load] if (x.second["status"] != pm_component_status_inactive["load"] && x.second["load_bus"] in keys(ref[:bus])))
	    ref[:shunt] = Dict(x for x in ref[:shunt] if (x.second["status"] != pm_component_status_inactive["shunt"] && x.second["shunt_bus"] in keys(ref[:bus])))
	    ref[:gen] = Dict(x for x in ref[:gen] if (x.second["gen_status"] != pm_component_status_inactive["gen"] && x.second["gen_bus"] in keys(ref[:bus])))
	    ref[:storage] = Dict(x for x in ref[:storage] if (x.second["status"] != pm_component_status_inactive["storage"] && x.second["storage_bus"] in keys(ref[:bus])))
	    ref[:switch] = Dict(x for x in ref[:switch] if (x.second["status"] != pm_component_status_inactive["switch"] && x.second["f_bus"] in keys(ref[:bus]) && x.second["t_bus"] in keys(ref[:bus])))
	    ref[:branch] = Dict(x for x in ref[:branch] if (x.second["br_status"] != pm_component_status_inactive["branch"] && x.second["f_bus"] in keys(ref[:bus]) && x.second["t_bus"] in keys(ref[:bus])))
	    ref[:dcline] = Dict(x for x in ref[:dcline] if (x.second["br_status"] != pm_component_status_inactive["dcline"] && x.second["f_bus"] in keys(ref[:bus]) && x.second["t_bus"] in keys(ref[:bus])))


	    ### setup arcs from edges ###
	    ref[:arcs_from] = [(i,branch["f_bus"],branch["t_bus"]) for (i,branch) in ref[:branch]]
	    ref[:arcs_to]   = [(i,branch["t_bus"],branch["f_bus"]) for (i,branch) in ref[:branch]]
	    ref[:arcs] = [ref[:arcs_from]; ref[:arcs_to]]

	    ref[:arcs_from_dc] = [(i,dcline["f_bus"],dcline["t_bus"]) for (i,dcline) in ref[:dcline]]
	    ref[:arcs_to_dc]   = [(i,dcline["t_bus"],dcline["f_bus"]) for (i,dcline) in ref[:dcline]]
	    ref[:arcs_dc]      = [ref[:arcs_from_dc]; ref[:arcs_to_dc]]

	    ref[:arcs_from_sw] = [(i,switch["f_bus"],switch["t_bus"]) for (i,switch) in ref[:switch]]
	    ref[:arcs_to_sw]   = [(i,switch["t_bus"],switch["f_bus"]) for (i,switch) in ref[:switch]]
	    ref[:arcs_sw] = [ref[:arcs_from_sw]; ref[:arcs_to_sw]]


	    ### bus connected component lookups ###
	    bus_loads = Dict((i, Int[]) for (i,bus) in ref[:bus])
	    for (i, load) in ref[:load]
	        push!(bus_loads[load["load_bus"]], i)
	    end
	    ref[:bus_loads] = bus_loads

	    bus_shunts = Dict((i, Int[]) for (i,bus) in ref[:bus])
	    for (i,shunt) in ref[:shunt]
	        push!(bus_shunts[shunt["shunt_bus"]], i)
	    end
	    ref[:bus_shunts] = bus_shunts

	    bus_gens = Dict((i, Int[]) for (i,bus) in ref[:bus])
	    for (i,gen) in ref[:gen]
	        push!(bus_gens[gen["gen_bus"]], i)
	    end
	    ref[:bus_gens] = bus_gens

	    bus_storage = Dict((i, Int[]) for (i,bus) in ref[:bus])
	    for (i,strg) in ref[:storage]
	        push!(bus_storage[strg["storage_bus"]], i)
	    end
	    ref[:bus_storage] = bus_storage

	    bus_arcs = Dict((i, Tuple{Int,Int,Int}[]) for (i,bus) in ref[:bus])
	    for (l,i,j) in ref[:arcs]
	        push!(bus_arcs[i], (l,i,j))
	    end
	    ref[:bus_arcs] = bus_arcs

	    bus_arcs_dc = Dict((i, Tuple{Int,Int,Int}[]) for (i,bus) in ref[:bus])
	    for (l,i,j) in ref[:arcs_dc]
	        push!(bus_arcs_dc[i], (l,i,j))
	    end
	    ref[:bus_arcs_dc] = bus_arcs_dc

	    bus_arcs_sw = Dict((i, Tuple{Int,Int,Int}[]) for (i,bus) in ref[:bus])
	    for (l,i,j) in ref[:arcs_sw]
	        push!(bus_arcs_sw[i], (l,i,j))
	    end
	    ref[:bus_arcs_sw] = bus_arcs_sw



	    ### reference bus lookup (a set to support multiple connected components) ###
	    ref_buses = Dict{Int,Any}()
	    for (k,v) in ref[:bus]
	        if v["bus_type"] == 3
	            ref_buses[k] = v
	        end
	    end

	    ref[:ref_buses] = ref_buses

	    if length(ref_buses) > 1
	        Memento.warn(_LOGGER, "multiple reference buses found, $(keys(ref_buses)), this can cause infeasibility if they are in the same connected component")
	    end

	    ### aggregate info for pairs of connected buses ###
	    if !haskey(ref, :buspairs)
	        ref[:buspairs] = calc_buspair_parameters(ref[:bus], ref[:branch], ref[:conductor_ids], haskey(ref, :conductors))
	    end

	    ### list all prosumers from producers and loads
	    ref[:prosumer] = Dict{Int,Any}()
	    for (i,prod) in ref[:gen]
	    	ref[:prosumer][i] = Dict(
	    								"costcoef" 	=> prod["cost"], 
	    								"pmin" 		=> prod["pmin"],
	    								"pmax" 		=> prod["pmax"],
	    								"type" 		=> "prod",
	    								"gen_index" => i,
	    						)
	    	if haskey(prod, "loc")
	    		ref[:prosumer][i]["loc"] = prod["loc"]
	    	else
	    		Memento.warn(_LOGGER, "Adding random localization to producer $i")
	    		ref[:prosumer][i]["loc"] = [rand(), rand()]
	    	end
	    	prod["prosumer_id"] = i # referencing prosumer in prod ref
	    end
	    k = maximum([i for (i,prod) in ref[:gen]]) + 1 
	    for (i,cons) in ref[:load]
	    	ref[:prosumer][k] = Dict(
	    								"costcoef" 	=> [], 
	    								"pmin" 		=> -cons["pd"],
	    								"pmax" 		=> -cons["pd"],
	    								"type" 		=> "cons",
	    								"load_index"=> i,
	    						)
	    	if haskey(cons, "loc")
	    		ref[:prosumer][k]["loc"] = cons["loc"]
	    	else
	    		Memento.warn(_LOGGER, "Adding random localization to consumer $i")
	    		ref[:prosumer][k]["loc"] = [rand(), rand()]
	    	end
	    	cons["prosumer_id"] = k # referencing prosumer in cons ref
	    	k+=1
	    end
	else
		Memento.error(_LOGGER, "Ref must have either 'prosumer' or 'bus' key.")
	end

	# Prosumer commercial links setup
	ref[:commercial_links] 	= Dict{Int,Any}()   # half full communication matrix: all producers connected to all consumers
	k = 1
	for (nprod,ncons) in Iterators.product(ref[:prosumer_prod],ref[:prosumer_cons])
		ref[:commercial_links][k] = Dict(
											"index" => k,
											"f_prosumer" => nprod,
											"t_prosumer" => ncons,
										)
		k+=1
	end

	ref[:commercial_arcs_from] 	= [(i, link["f_prosumer"], link["t_prosumer"]) for (i,link) in ref[:commercial_links]]
	ref[:commercial_arcs_to] 	= [(i, link["t_prosumer"], link["f_prosumer"]) for (i,link) in ref[:commercial_links]]
	ref[:commercial_arcs] 		= [ref[:commercial_arcs_from]; ref[:commercial_arcs_to]]


	ref[:prosumer_arcs] = Dict((i, Tuple{Int,Int,Int}[]) for (i,prosumer) in ref[:prosumer])
	for (arc, i, j) in ref[:commercial_arcs]
		push!(ref[:prosumer_arcs][i], (arc, i, j))
	end

	# convergence threshold
	ref[:ε_prim] = 1e-4
	ref[:ε_dual] = 1e-4

	ref
end




"Add local data to local ref"
function ref_add_agent_core!(ref::Dict{Symbol,<:Any})
	nothing
end