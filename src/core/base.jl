"""
The `def` macro is used to build other macros that can insert the same block of
julia code into different parts of a program.  In InfrastructureModels packages
this is macro is used to generate a standard set of fields inside a model type
hierarchy.
"""
macro def(name, definition)
    return quote
        macro $(esc(name))()
            esc($(Expr(:quote, definition)))
        end
    end
end

"type of the local problem"
abstract type DEAbstractContent end
"root of the infrastructure model formulation type hierarchy"
abstract type AbstractDecElecContainer end


"a macro for adding the standard DecElec fields to a type definition"
DecElec.@def de_fields begin
    content::DEAbstractContent

    ref::Dict{Symbol,<:Any}
    data::Dict{String,<:Any}

    var::Dict{Symbol,<:Any}
    sol::Dict{String,<:Any}

    mon::Dict{String,<:Any}

    # Extension dictionary
    # Extensions should define a type to hold information particular to
    # their functionality, and store an instance of the type in this
    # dictionary keyed on an extension-specific symbol
    ext::Dict{Symbol,<:Any}
end



# default generic constructor
function InitializeDecElecModel(DecElecModel::Type, local_problem_model::DEAbstractContent, data::Dict{String,<:Any}; 
    ext = Dict{Symbol,Any}(), kwargs...)
    @assert DecElecModel <: AbstractDecElecContainer

    ref = ref_initialize(data) # reference data

    var = Dict{Symbol,Any}()
    sol = Dict{String,Any}()
    mon = Dict{String,Any}()

    dem = DecElecModel(
        local_problem_model,
        ref,
        data,
        var,
        sol,
        mon,
        ext
    )

    return dem
end

"""
Given a data dictionary following the Infrastructure Models conventions, builds
an initial "ref" dictionary converting strings to symbols and component
keys to integers. 
"""
function ref_initialize(data::Dict{String,<:Any})
    ref = Dict{Symbol,Any}()
    for (key, item) in data
        if isa(item, Dict{String,Any}) && _iscomponentdict(item)
            item_lookup = Dict{Int,Any}([(parse(Int, k), v) for (k,v) in item])
            ref[Symbol(key)] = item_lookup
        else
            ref[Symbol(key)] = item
        end
    end

    return ref
end

ref(demo::AbstractDecElecContainer, name::Symbol) = demo.ref[name]
ref(demo::AbstractDecElecContainer, name::Symbol, idx) = demo.ref[name][idx]

var(demo::AbstractDecElecContainer, name::Symbol) = demo.var[name]
var(demo::AbstractDecElecContainer, name::Symbol, idx) = demo.var[name][idx]

sol(demo::AbstractDecElecContainer, name::String) = demo.sol[name]
sol(demo::AbstractDecElecContainer, name::String, idx) = demo.sol[name][idx]

""
function instantiate_model(data::Dict{String,<:Any}, model_type::Type, 
    content::DEAbstractContent, build_method, ref_add_core!; 
    ref_extensions=[], iter_max=1000, kwargs...)
    # NOTE, this model constructor will build the ref dict using the latest info from the data

    start_time = time()
    demo = InitializeDecElecModel(model_type, content, data; kwargs...)
    Memento.debug(_LOGGER, "initialize decentralized model time: $(time() - start_time)")

    start_time = time()
    ref_add_core!(demo.ref)
    for ref_ext! in ref_extensions
        ref_ext!(demo.ref, demo.data)
    end
    Memento.debug(_LOGGER, "build ref time: $(time() - start_time)")

    extend_model(demo)

    start_time = time()
    build_method(demo, iter_max)
    Memento.debug(_LOGGER, "build method time: $(time() - start_time)")

    return demo
end


