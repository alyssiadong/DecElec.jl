function visualize(demo::AbstractDecElecProblem, symbol::Symbol; logscale = false, kwargs...)
	if haskey(demo.mon, String(symbol))
		data = logscale ? log10.(demo.mon[String(symbol)][0]) : demo.mon[String(symbol)][0]
		trace = scatter(y=data)
	else
		trace = visualize_agents(demo, symbol; logscale = logscale, kwargs...)
	end
	plot(trace)
end

function visualize_agents(demo::AbstractP2PProblem, symbol::Symbol; logscale = false, kwargs...)
	trace_array = GenericTrace[]
	for (n, agent) in demo.content.prosumers
		if haskey(agent.mon, String(symbol))
			for (idx, data_raw) in agent.mon[String(symbol)]
				data = logscale ? log10.(data_raw) : data_raw
				trace = scatter(y = data, name = idx)
				push!(trace_array, trace)
			end
		else
			break
		end
	end

	return trace_array
end