"Group of agents specific functions"

"extend_model: Instantiate each agent of the group of agents (OR does nothing if the model is already a singular agent)"
extend_model(demo::AbstractDecElecProblem) = Memento.error(_LOGGER, "Not coded yet.")

function extend_model(demo::AbstractP2PProblem; ref_agent_extensions=[])
    for (n, prosumer) in ref(demo, :prosumer)
    	data_agent = Dict(
    						"prosumer" 	=> prosumer,
    						"commercial_arcs" => ref(demo, :prosumer_arcs, n), 
    		)
    	data_agent["commercial_arcs_from"] 	= [arc for arc in data_agent["commercial_arcs"] if arc∈ref(demo, :commercial_arcs_from)]
    	data_agent["commercial_arcs_to"]   	= [arc for arc in data_agent["commercial_arcs"] if arc∈ref(demo, :commercial_arcs_to)]
        data_agent["id_agent"] = prosumer["id_agent"]

    	model = instantiate_model(data_agent, P2PAgent, OSQPModel(OSQP.Model()), build_agent, ref_add_agent_core!)
    	demo.content.agents[n] = model
    end
end


"build_group: residuals variables setup"
build_group(demo::AbstractDecElecProblem, iter_max::Int) = nothing

function build_group(demo::AbstractP2PProblem, iter_max::Int)
    demo.var[:primal_res]   = [0]
    demo.var[:dual_res]     = [0]

    demo.sol["primal_res"]  = Dict( 0 => Inf)
    demo.sol["dual_res"]    = Dict( 0 => Inf)

    mon_initialize(demo, iter_max)
end