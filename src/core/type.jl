#########################################################################################
### Problem container types
#########################################################################################

abstract type AbstractDecElecProblem <: AbstractDecElecContainer end

# Peer to peer problem abstract type
abstract type AbstractP2PProblem <: AbstractDecElecProblem end

# OPF problem abstract type
abstract type AbstractOPFProblem <: AbstractDecElecProblem end

# Endogenous problem abstract type
abstract type AbstractEndogenousProblem <: AbstractDecElecProblem end


# Peer to peer problem agent struct
struct P2PProblem <: AbstractP2PProblem @de_fields end 

# OPF problem struct
struct OPFNodesProblem <: AbstractOPFProblem @de_fields end
struct OPFLinksProblem <: AbstractOPFProblem @de_fields end

# OPF problem struct
struct OPFNodesProblem <: AbstractOPFProblem @de_fields end


#########################################################################################
### Agent container types
#########################################################################################

abstract type AbstractDecElecAgent <: AbstractDecElecContainer end

abstract type AbstractP2PAgent 	<: 	AbstractDecElecAgent end
abstract type AbstractOPFAgent 	<: 	AbstractDecElecAgent end

struct P2PAgent 		<: 	AbstractP2PAgent @de_fields end
struct EndoP2PAgent 	<: 	AbstractP2PAgent @de_fields end
struct DecEndoP2PAgent 	<: 	AbstractP2PAgent @de_fields end

struct OPFNodeAgent 	<: 	AbstractOPFAgent @de_fields end
struct OPFLinkAgent 	<: 	AbstractOPFAgent @de_fields end
struct EndoSOAgent 		<: 	AbstractOPFAgent @de_fields end
struct DecEndoSOAgent 	<: 	AbstractOPFAgent @de_fields end

#########################################################################################
### Problem types inside container : either JuMP/OSQP or dictionnary of agents
#########################################################################################

struct JuMPModel <: DEAbstractContent 
	model::JuMP.AbstractModel
end
struct OSQPModel <: DEAbstractContent 
	model::OSQP.Model
end

struct GroupOfAgents <: DEAbstractContent 
	agents::Dict{Int, <:AbstractDecElecAgent}

	function GroupOfAgents(agents::Dict{Int, <:AbstractDecElecAgent} = Dict{Int, AbstractDecElecAgent}())
		new(agents)
	end
end
