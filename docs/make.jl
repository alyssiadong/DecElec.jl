using DecElec
using Documenter

DocMeta.setdocmeta!(DecElec, :DocTestSetup, :(using DecElec); recursive=true)

makedocs(;
    modules=[DecElec],
    authors="Alyssia Dong <alyssia.dong@ens-rennes.fr> and contributors",
    repo="https://gitlab.com/alyssiadong/DecElec.jl/blob/{commit}{path}#{line}",
    sitename="DecElec.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://alyssiadong.gitlab.io/DecElec.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
