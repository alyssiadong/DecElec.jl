# DecElec

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://alyssiadong.gitlab.io/DecElec.jl/dev)
[![Build Status](https://gitlab.com/alyssiadong/DecElec.jl/badges/master/pipeline.svg)](https://gitlab.com/alyssiadong/DecElec.jl/pipelines)
[![Coverage](https://gitlab.com/alyssiadong/DecElec.jl/badges/master/coverage.svg)](https://gitlab.com/alyssiadong/DecElec.jl/commits/master)
